import org.fnlp.nlp.cn.CNFactory;
import org.fnlp.nlp.cn.ner.TimeNormalizer;
import org.fnlp.nlp.cn.ner.TimeUnit;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by likid on 19/12/2016.
 */
public class Demo {

    private CNFactory factory;

    public Demo() throws Exception {
        // 创建中文处理工厂对象，并使用“models”目录下的模型文件初始化
        factory = CNFactory.getInstance("models");
    }

    public static void main(String[] args) throws Exception {

        String sentence = "我有一点饿，打算一点去711吃车仔面";//"三点一线";//"周杰伦出生于台湾，生日为79年1月18日，他曾经的绯闻女友是蔡依林。";

        Demo test = new Demo();
        test.segTest(sentence);
        test.tagTest(sentence);
        test.nerTest(sentence);

//        test.tagTimeTest();
    }

    public void segTest(String sentence) {
        // 使用分词器对中文句子进行分词，得到分词结果
        String[] words = factory.seg(sentence);

        // 打印分词结果
        for (String word : words) {
            System.out.print(word + " ");
        }
        System.out.println();
    }

    public void tagTest(String sentence) {
        // 使用标注器对中文句子进行标注，得到标注结果
        String tags = factory.tag2String(sentence);
        // 显示标注结果
        System.out.println(tags);

        String[][] tags1 = factory.tag(sentence);
        System.out.println(Arrays.deepToString(tags1));
    }

    public void tagTimeTest() {
//        String s1 = "今天下午我打算去找小明吃饭，大概在五点一刻左右";
//        String s2 = "下周三早上十点零五分钟有个会议，可能会持续三个小时";
//
//        String[] sentences = new String[] {s1, s2};
//        String[] tagResult = factory.tag(sentences);
//
//        System.out.println(Arrays.deepToString(tagResult));
//
//        System.out.println(factory.tag2String(s1));
//        System.out.println(factory.tag2String(s2));

//        String target = "08年北京申办奥运会，8月8号开幕式，九月十八号闭幕式。" +
//                "一年后的七月21号发生了件大事。" +
//                "今天我本想去世博会，但是人太多了，直到晚上9点一刻人还是那么多。" +
//                "考虑到7/4/16 19:01和后天14:05人还是那么多，决定下周日晚上10:15:21再去。" +
//                "恢复自我美德的时代，充满美丽、希望、挑战的上，我们所处的敏感时期会有一个人在午时出现拯救世界。" +
//                "国庆节晚上八点" +
//                "我有一点饿";
        String target = "三点一线";
        TimeNormalizer normalizer;
        normalizer = new TimeNormalizer("models/time.m");
        normalizer.parse(target);
        TimeUnit[] unit = normalizer.getTimeUnit();
        for(int i = 0; i < unit.length; i++){
            System.out.println(unit[i]);
        }
    }

    public void nerTest(String sentence) {
        // 使用标注器对包含实体名的句子进行标注，得到结果
        HashMap<String, String> nerMap = factory.ner(sentence);
        // 显示标注结果
        System.out.println(nerMap);
    }
}
